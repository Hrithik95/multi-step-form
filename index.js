const pageOne = document.getElementById('page-1');
const pageTwo = document.getElementById('page-2');
const pageThree = document.getElementById('page-3');
const pageFour = document.getElementById('page-4');
const pageFive = document.getElementById('page-5');
const toggleSwitch = document.getElementById("toggle-switch");
const freeMessage = document.getElementsByClassName('message');
const addOnPrice = document.getElementsByClassName('add-on-price');
const buttonOne = document.getElementById('button-1');
const buttonTwo = document.getElementById('button-2');
const buttonThree = document.getElementById('button-3');
const buttonFour = document.getElementById('button-4');
const backBtnOne = document.getElementById('back-btn-1');
const backBtnTwo = document.getElementById('back-btn-2');
const backBtnThree = document.getElementById('back-btn-3');
const pageTwoBoxes = document.querySelectorAll(".page-2-boxes > div");
const priceWarning = document.getElementById('warning-message');
const addOnItem = document.getElementsByClassName('add-on-item');
const countOne = document.getElementById('count-1');
const countTwo = document.getElementById('count-2');
const countThree = document.getElementById('count-3');
const countFour = document.getElementById('count-4');
const subscriptionType = document.getElementById('subscription-type');
const subscriptionPrice = document.getElementById('subscription-price');
const changePlan = document.getElementById('change-plan');
const addOnsItems = document.getElementById('add-ons');
const totalPrice = document.getElementById('total-price-text');
const totalCost = document.getElementById('total-cost');

//form validation
function setError(id, error) {
    //sets error inside the tag id
    if (id === 'name') {
        document.getElementsByClassName('form-error')[0].textContent = error;
    }
    if (id === 'email') {
        document.getElementsByClassName('form-error')[1].textContent = error;
    }
    if (id === 'phone') {
        document.getElementsByClassName('form-error')[2].textContent = error;
    }
}

function validateForm() {
    let returnVal = true;
    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const phone = document.getElementById('phone').value;

    setError('name', '');
    setError('email', '');
    setError('phone', '');


    const nonAlphabetNameRegex = /^[a-zA-Z ]+$/;
    if (name.length === 0) {
        document.querySelector("#name").style.border = "1px solid red";
        setError('name', '*Name is required');
        returnVal = false;
    } else if (name.length < 3) {
        document.querySelector("#name").style.border = "1px solid red";
        setError('name', '*Length of name is too sort');
    } else if (nonAlphabetNameRegex.test(name) === false) {
        document.querySelector("#name").style.border = "1px solid red";
        setError('name', '*Name should contain only alphabets');
        returnVal = false;
    } else {
        document.querySelector("#name").style.border = "1px solid lightgray";
    }

    if (email.length === 0) {
        document.querySelector("#email").style.border = "1px solid red";
        setError('email', '*Email is required');
        returnVal = false;
    } else if (email.includes("@") === false) {
        document.querySelector("#email").style.border = "1px solid red";
        setError('email', '*Enter a valid Email Address');
        returnVal = false;
    } else {
        document.querySelector("#email").style.border = "1px solid lightgray";
    }

    if (phone.length === 0) {
        document.querySelector("#phone").style.border = "1px solid red";
        setError('phone', '*Phone number is required');
        returnVal = false;
    } else if (phone.length !== 10) {
        document.querySelector("#phone").style.border = "1px solid red";
        setError('phone', '*Phone Number should be of 10 digits');
        returnVal = false;
    } else if (isNaN(Number(phone))) {
        document.querySelector("#phone").style.border = "1px solid red";
        setError('phone', '*Enter a valid Phone Number');
        returnVal = false;
    } else {
        document.querySelector("#phone").style.border = "1px solid lightgray";
    }

    return returnVal;
}

// Add event listener to next step buttons
buttonOne.addEventListener('click', () => {
    console.log('inside btn');
    const formValidation = validateForm();
    if (formValidation) {
        console.log('inside validation');
        pageOne.style.display = 'none';
        pageTwo.style.display = 'flex';
        countOne.style.backgroundColor = 'transparent';
        countTwo.style.backgroundColor = '#BEE1FF';
        countOne.style.color = '#FFF';
        countTwo.style.color = 'black';
    }
});


buttonTwo.addEventListener("click", function () {
    let selectedPlan = null;
    for (let planIndex = 0; planIndex < pageTwoBoxes.length; planIndex++) {
        if (pageTwoBoxes[planIndex].classList.contains("selected")) {
            selectedPlan = pageTwoBoxes[planIndex];
            break;
        }
    }
    if (selectedPlan) {
        pageTwo.style.display = "none";
        pageThree.style.display = "flex";
        countTwo.style.backgroundColor = 'transparent';
        countThree.style.backgroundColor = '#BEE1FF';
        countTwo.style.color = '#FFF';
        countThree.style.color = 'black';
        priceWarning.textContent = '';
        priceWarning.style.color = 'none';
    } else {
        priceWarning.textContent = 'Please select a plan to move forward';
        priceWarning.style.color = 'blue';
    }
});

//Select subscription
for (let planIndex = 0; planIndex < pageTwoBoxes.length; planIndex++) {
    pageTwoBoxes[planIndex].addEventListener("click", function () {
        const previouslySelected = document.querySelector(".page-2-boxes .selected");
        if (previouslySelected) {
            previouslySelected.classList.remove("selected");
            previouslySelected.style.border = "1px solid lightgray";
            previouslySelected.style.backgroundColor = '#FFF';
        }
        pageTwoBoxes[planIndex].classList.add("selected");
        pageTwoBoxes[planIndex].style.border = "1px solid #847FB9";
        pageTwoBoxes[planIndex].style.backgroundColor = '#F8F9FE';
        const planTitle = this.querySelector(".plan-title");
        const planPricing = this.querySelector(".plan-price");
        localStorage.setItem("plan-type", planTitle.textContent);
        localStorage.setItem("plan-price", planPricing.textContent);
    });
}

//Select Add-ons
let addOnObjects = {};

for (let addOnIndex = 0; addOnIndex < addOnItem.length; addOnIndex++) {
    addOnItem[addOnIndex].addEventListener('change', () => {
        let addOnItemSelected = addOnItem[addOnIndex].checked;
        let parentElement = addOnItem[addOnIndex].parentNode;
        let siblingElement = addOnItem[addOnIndex].nextElementSibling;
        if (addOnItemSelected) {
            console.log('item added to object');
            addOnObjects[siblingElement.firstElementChild.textContent] = parentElement.nextElementSibling.textContent;
            parentElement.parentNode.style.border = "1px solid #847FB9";
            parentElement.parentNode.style.backgroundColor = '#F8F9FE';
            console.log(addOnObjects);
        } else {
            delete addOnObjects[siblingElement.firstElementChild.textContent];
            console.log('item removed', addOnObjects);
            parentElement.parentNode.style.border = "1px solid lightgray";
            parentElement.parentNode.style.backgroundColor = '#FFF';
        }
    });
}

buttonThree.addEventListener('click', () => {
    pageThree.style.display = 'none';
    pageFour.style.display = 'flex';
    subscriptionType.textContent = localStorage.getItem('plan-type');
    subscriptionPrice.textContent = localStorage.getItem('plan-price');
    countFour.style.backgroundColor = '#BEE1FF';
    countThree.style.backgroundColor = 'transparent';
    countFour.style.color = 'black';
    countThree.style.color = '#FFF';
    let totalSubscriptionPrice = parseInt(localStorage.getItem('plan-price').substring(1, localStorage.getItem("plan-price").indexOf("/")));
    let subscriptionDuration = localStorage.getItem('plan-price').substring(localStorage.getItem("plan-price").indexOf("/") + 1, localStorage.getItem("plan-price").length);
    for (let key in addOnObjects) {
        let addOnContainer = document.createElement('div');
        addOnContainer.classList.add('add-on-container');
        let addOnType = document.createElement('div');
        addOnType.classList.add('add-on-type');
        let addOnPricing = document.createElement('div');
        addOnPricing.classList.add('add-on-price');
        addOnType.textContent = key;
        addOnPricing.textContent = addOnObjects[key];
        addOnContainer.append(addOnType);
        addOnContainer.append(addOnPricing);
        addOnsItems.append(addOnContainer);
        totalSubscriptionPrice += parseInt(addOnObjects[key].substring(2, addOnObjects[key].indexOf('/')));
    }
    if (subscriptionDuration.startsWith('m')) {
        totalCost.textContent = `$${totalSubscriptionPrice}/mo`;
        totalPrice.textContent=`Total(per month)`;
        subscriptionType.textContent = localStorage.getItem('plan-type')+"(monthly)";
    } else {
        totalCost.textContent = `$${totalSubscriptionPrice}/yr`;
        subscriptionType.textContent = localStorage.getItem('plan-type')+"(yearly)";
        totalPrice.textContent=`Total(per year)`;
    }
});

changePlan.addEventListener('click', () => {
    pageFour.style.display = 'none';
    pageTwo.style.display = 'flex';
    countTwo.style.backgroundColor = '#BEE1FF';
    countFour.style.backgroundColor = 'transparent';
    countTwo.style.color = 'black';
    countFour.style.color = '#FFF';
    while (addOnsItems.firstChild) {
        addOnsItems.removeChild(addOnsItems.firstChild);
    }
    for (let addOnIndex = 0; addOnIndex < addOnItem.length; addOnIndex++) {
        addOnItem[addOnIndex].checked = false;
        addOnItem[addOnIndex].parentNode.parentNode.style.border = "1px solid lightgray";
        addOnItem[addOnIndex].parentNode.parentNode.style.backgroundColor = '#FFF';
    }
    addOnObjects = {};
});

buttonFour.addEventListener('click', () => {
    pageFour.style.display = 'none';
    pageFive.style.display = 'flex';
});

//Add event listener to go back buttons
backBtnOne.addEventListener('click', () => {
    pageOne.style.display = 'flex';
    pageTwo.style.display = 'none';
    countTwo.style.backgroundColor = 'transparent';
    countOne.style.backgroundColor = '#BEE1FF';
    countOne.style.color = 'black';
    countTwo.style.color = '#FFF';
});

backBtnTwo.addEventListener('click', () => {
    pageThree.style.display = 'none';
    pageTwo.style.display = 'flex';
    countThree.style.backgroundColor = 'transparent';
    countTwo.style.backgroundColor = '#BEE1FF';
    countTwo.style.color = 'black';
    countThree.style.color = '#FFF';
});

backBtnThree.addEventListener('click', () => {
    pageFour.style.display = 'none';
    pageThree.style.display = 'flex';
    while (addOnsItems.firstChild) {
        addOnsItems.removeChild(addOnsItems.firstChild);
    }
    countFour.style.backgroundColor = 'transparent';
    countThree.style.backgroundColor = '#BEE1FF';
    countThree.style.color = 'black';
    countFour.style.color = '#FFF';
});

toggleSwitch.addEventListener("change", function (event) {
    let pricing = document.getElementsByClassName("plan-price");
    if (toggleSwitch.checked) {
        addOnObjects = {};
        console.log(subscriptionType);
        subscriptionPrice.textContent = localStorage.getItem('plan-price');
        while (addOnsItems.firstChild) {
            addOnsItems.removeChild(addOnsItems.firstChild);
        }
        for (let addOnIndex = 0; addOnIndex < addOnItem.length; addOnIndex++) {
            addOnItem[addOnIndex].checked = false;
            addOnItem[addOnIndex].parentNode.parentNode.style.border = "1px solid lightgray";
            addOnItem[addOnIndex].parentNode.parentNode.style.backgroundColor = '#FFF';
        }
        pricing[0].textContent = "$90/yr";
        pricing[1].textContent = "$120/yr";
        pricing[2].textContent = "$150/yr";
        if (localStorage.getItem('plan-type') === "Arcade") {
            localStorage.setItem('plan-price', pricing[0].textContent);
        } else if (localStorage.getItem('plan-type') === "Advanced") {
            localStorage.setItem('plan-price', pricing[0].textContent);
        } else {
            localStorage.setItem('plan-price', pricing[2].textContent);
        }

        for (let index = 0; index < freeMessage.length; index++) {
            freeMessage[index].textContent = "2 months free";
        }
        addOnPrice[0].textContent = '+$10/yr';
        addOnPrice[1].textContent = '+$20/yr';
        addOnPrice[2].textContent = '+$20/yr';
    }
    else {
        pricing[0].textContent = "$9/mo";
        pricing[1].textContent = "$12/mo";
        pricing[2].textContent = "$15/mo";
        if (localStorage.getItem('plan-type') === "Arcade") {
            localStorage.setItem('plan-price', pricing[0].textContent);
        } else if (localStorage.getItem('plan-type') === "Advanced") {
            localStorage.setItem('plan-price', pricing[0].textContent);
        } else {
            localStorage.setItem('plan-price', pricing[2].textContent);
        }
        for (let index = 0; index < freeMessage.length; index++) {
            freeMessage[index].textContent = "";
        }
        addOnPrice[0].textContent = '+$1/mo';
        addOnPrice[1].textContent = '+$2/mo';
        addOnPrice[2].textContent = '+$2/mo';
        while (addOnsItems.firstChild) {
            addOnsItems.removeChild(addOnsItems.firstChild);
        }
        for (let addOnIndex = 0; addOnIndex < addOnItem.length; addOnIndex++) {
            addOnItem[addOnIndex].checked = false;
            addOnItem[addOnIndex].parentNode.parentNode.style.border = "1px solid lightgray";
            addOnItem[addOnIndex].parentNode.parentNode.style.backgroundColor = '#FFF';
        }
    }
});

window.addEventListener('load', () => {
    toggleSwitch.checked = false;
    for (let addOnIndex = 0; addOnIndex < addOnItem.length; addOnIndex++) {
        addOnItem[addOnIndex].checked = false;
    }

});